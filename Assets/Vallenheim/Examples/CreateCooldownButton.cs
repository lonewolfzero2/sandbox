﻿using UnityEngine;
using System.Collections;

public class CreateCooldownButton : MonoBehaviour
{

    public CooldownButtonModel[] CooldownButtonModels;

	void Start () 
    {
	    foreach (var cooldownButtonModel in CooldownButtonModels)
	    {
            CooldownButtonManager.Instance.CreateNewButton(cooldownButtonModel);
	    }
	}
}