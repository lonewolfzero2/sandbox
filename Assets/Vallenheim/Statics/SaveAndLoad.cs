﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Vallenheim.Statics
{
    public static class SaveAndLoad
    {
        public const string FILE_NAME = "/SaveFile.xml";
        public static void SaveGame(string path, SaveDataCollection model)
        {
            var serializer = new XmlSerializer(typeof (SaveDataCollection));

            var sw = new StreamWriter(path + FILE_NAME);
            serializer.Serialize(sw, model);
            sw.Close();
        }

        public static SaveDataCollection LoadGame(string path)
        {
            try
            {
                var sr = new StreamReader(path + FILE_NAME);
                var reader = new StringReader(sr.ReadToEnd());
                var deserializer = new XmlSerializer(typeof(SaveDataCollection));
                var xmlFromText = new XmlTextReader(reader);
                var saveDataCollection = (SaveDataCollection)deserializer.Deserialize(xmlFromText);
                reader.Close();
                return saveDataCollection;
            }
            catch
            {
                return new SaveDataCollection(0);
            }
        }
    }

    [Serializable]
    public class GameObjectSaveData
    {
        [XmlAttribute]
        public string Id { get; set; }
        [XmlElement]
        public Vector3 Position { get; set; }
        [XmlElement]
        public Quaternion Rotation { get; set; }

        public GameObjectSaveData()
        {
            Position = Vector3.zero;
            Rotation = Quaternion.identity;
        }

        public GameObjectSaveData(string id, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
        {
            Id = id;
            Position = position;
            Rotation = rotation;
        }
    }

    [Serializable]
    public class SaveDataCollection
    {
        [XmlElement]
        public int Score { get; set; }

        [XmlElement(ElementName = "SaveData", Type = typeof(GameObjectSaveData))]
        public List<GameObjectSaveData> SaveData { get; set; }

        public SaveDataCollection(int score)
        {
            SaveData = new List<GameObjectSaveData>();

            Score = Score;
        }

    }
}