﻿using UnityEngine;
using System.Collections;

public class GUIButtonClickTest : MonoBehaviour
{
    private void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 25), "Test"))
        {
            Debug.Log("Testing");
        }
        else
        {
            Debug.Log("Finished Testing");
        }
    }
}
