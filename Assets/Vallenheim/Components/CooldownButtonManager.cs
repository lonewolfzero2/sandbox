﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Vallenheim.Components;

public class CooldownButtonManager : MonoBehaviour
{
    public static CooldownButtonManager Instance { get { return _instance; } }
    private static CooldownButtonManager _instance;

    public GUIStyle ButtonStyle;
    public GameObject CooldownButtonTemplate;
    public Camera Camera;

    private Vector3 _screenZeroPoint;

    private void Awake()
    {
        if (Camera == null)
            throw new Exception("Failed to find Camera, please attach an orthographic camera to manage the cooldown visuals");

        if (CooldownButtonTemplate == null)
            throw new Exception("Failed to find a Template for Overlay Buttons, there should be an example of it :D");

        if (_instance == null)
            _instance = this;
        else
            throw new Exception("There should be only one Cooldown Button Manager :D");

        _screenZeroPoint = Camera.ScreenToWorldPoint(Vector3.zero);
    }
    
    private readonly List<CooldownButtonController> _cooldownButtons = new List<CooldownButtonController>();
    public CooldownButtonController CreateNewButton(CooldownButtonModel cooldownButtonModel)
    {
        // Create new instance of the button
        var overlayObject = Instantiate(CooldownButtonTemplate) as GameObject;

        // Manage Position and size of the new button
        var convertedSize = Camera.ScreenToWorldPoint(new Vector3(cooldownButtonModel.Rect.width, cooldownButtonModel.Rect.height, 0f));
        overlayObject.transform.localScale = convertedSize - _screenZeroPoint;
        var convertedPosition = Camera.ScreenToWorldPoint(new Vector3(cooldownButtonModel.Rect.x, cooldownButtonModel.Rect.y, 0f));
        overlayObject.transform.localPosition = new Vector3(convertedPosition.x, -convertedPosition.y, 0);

        var cooldownButtonController = overlayObject.GetComponent<CooldownButtonController>();
        cooldownButtonController.Texture = cooldownButtonModel.ButtonTexture;
        cooldownButtonController.Duration = cooldownButtonModel.Duration;
        cooldownButtonController.Initiate(cooldownButtonModel.Rect);
        _cooldownButtons.Add(cooldownButtonController);

        return cooldownButtonController;
    }

    private void OnGUI()
    {
        foreach (var cooldownButton in _cooldownButtons)
        {
            if (GUI.Button(cooldownButton.Rect, "", ButtonStyle))
            {
                cooldownButton.Click();
            }
        }
    }
}

[Serializable]
public class CooldownButtonModel
{
    public Rect Rect;
    public Texture ButtonTexture;
    public float Duration;
}
