﻿using System;
using System.Collections;
using UnityEngine;

namespace Vallenheim.Components
{
    public class CooldownButtonController : MonoBehaviour
    {
        public Action OnClicked;

        public Texture Texture
        {
            set { SkillButtonRenderer.material.mainTexture = value; }
        }

        public float Duration;
        public Rect Rect { get; private set; }

        public Renderer MainOverlayRenderer;
        public Renderer SkillButtonRenderer;

        private bool _isInitiated;
        public void Initiate(Rect rect)
        {
            if (_isInitiated)
                throw new Exception("You only need to instantiate once, please make sure you do, it may lead to unwanted behaviour");

            _isInitiated = true;
            _isActive = true;

            Rect = rect;
        }

        public void StartCooldownAnimation(float duration)
        {
            StartCoroutine(AnimateCooldown(Time.time, duration));
        }

        private IEnumerator AnimateCooldown(float startTime, float duration)
        {
            while (Time.time - startTime < duration)
            {
                var deltaSinceStart = Time.time - startTime;
                MainOverlayRenderer.material.SetFloat("_Cutoff", Mathf.InverseLerp(0, duration, deltaSinceStart));

                yield return null;
            }
            MainOverlayRenderer.material.SetFloat("_Cutoff", 1);
        }

        private bool _isActive;
        public void Click()
        {
            if (!_isActive) return;

            if (OnClicked != null)
                OnClicked();    

            StartCooldownAnimation(Duration);
        }

        
    }
}
