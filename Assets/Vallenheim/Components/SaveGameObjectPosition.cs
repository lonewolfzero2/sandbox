﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Vallenheim.Statics;

namespace Vallenheim.Components
{
    public class SaveGameObjectPosition : MonoBehaviour 
    {
        private void Start()
        {
            var saveDataCollection = SaveAndLoad.LoadGame(Application.dataPath);
            var savedObjects = GameObject.FindGameObjectsWithTag("Saved");
            var savedObjectsDictionary = new Dictionary<string, GameObject>();
            foreach (var savedObject in savedObjects)
            {
                savedObjectsDictionary.Add(savedObject.name, savedObject);
            }

            foreach (var gameObjectSaveData in saveDataCollection.SaveData)
            {
                var isSavedObjectFound = savedObjectsDictionary.ContainsKey(gameObjectSaveData.Id);
                if (!isSavedObjectFound)
                    throw new Exception(string.Format("Kaga ada GameObject yang namanya: {0}, kalo namanya ada, coba dicek Tag-nya: {1}", gameObjectSaveData.Id, "Saved"));

                savedObjectsDictionary[gameObjectSaveData.Id].transform.position = gameObjectSaveData.Position;
                savedObjectsDictionary[gameObjectSaveData.Id].transform.rotation = gameObjectSaveData.Rotation;
            }
        }

        private void Update () 
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                var newSaveDataCollection = new SaveDataCollection(500)
                {
                    SaveData = new List<GameObjectSaveData>()
                };
                foreach (var go in GameObject.FindGameObjectsWithTag("Saved"))
                {
                    newSaveDataCollection.SaveData.Add(new GameObjectSaveData(go.name, go.transform.localPosition, go.transform.localRotation));
                }

                SaveAndLoad.SaveGame(Application.dataPath, newSaveDataCollection);
            }
        }
    }
}
